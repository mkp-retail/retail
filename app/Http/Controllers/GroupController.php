<?php

namespace App\Http\Controllers;

use App\Models\Group;
use Illuminate\Http\Request;
use App\Http\Requests\GroupRequest;
use DB;

class GroupController extends Controller
{
    public function index()
    {
        $dataRegion = DB::table('region')->get();
        return view('contents.group.index',compact('dataRegion'));
    }
    public function all(Request $request)
    {
        abort_if(!$request->header('X-XSRF-TOKEN'), 404);
        return Group::join('region', 'region.id', '=', 'group.region_id')
        ->get([
            'group.*',
            'region.region_name'
        ]);
    }
    public function store(GroupRequest $request)
    {
        Group::create($request->validated());
        return true;
    }
    public function update(GroupRequest $request, Group $group)
    {
        $group->update($request->validated());
        return true;
    }
    public function destroy(Group $group)
    {
        $group->delete();
        return true;
    }
}
