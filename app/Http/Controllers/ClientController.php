<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;
use App\Http\Requests\ClientRequest;


class ClientController extends Controller
{
    public function index()
    {
        return view('contents.client.index');
    }
    public function all(Request $request){
        abort_if(!$request->header('X-XSRF-TOKEN'), 404);
        return Client::get();
    }
    public function store(ClientRequest $request)
    {
        Client::create($request->validated());
        return true;
    }  
    public function update(ClientRequest $request, Client $client)
    {
        $client->update($request->validated());
        return true;
    }
    public function destroy(Client $client)
    {
        $client->delete();
        return true;
    }
}
