<?php

namespace App\Http\Controllers;

use App\Models\{Merchant, Group};
use Illuminate\Http\Request;
use App\Http\Requests\MerchantRequest;

class MerchantController extends Controller
{
    public function index()
    {
        $group = Group::get(['id', 'group_name']);
        return view('contents.merchant.index', compact('group'));
    }
    public function all(Request $request)
    {
        $mainData = Merchant::join('group', 'merchant.group_id', '=', 'group.id')
            ->join('indonesia_villages', 'merchant.village_id', '=', 'indonesia_villages.id')
            ->join('indonesia_districts', 'indonesia_districts.id', '=', 'indonesia_villages.district_id')
            ->join('indonesia_cities', 'indonesia_cities.id', '=', 'indonesia_districts.city_id')
            ->select('merchant.*', 'group.group_name',
                'indonesia_cities.name as uraiankota',
                'indonesia_districts.name as uraiankecamatan',
                'indonesia_villages.name as uraiandesa'
            );

        return datatables()->of($mainData)
            ->addIndexColumn()
            ->make();
    }
    public function store(MerchantRequest $request)
    {
        Merchant::create($request->validated());
        return true;
    }
    public function update(MerchantRequest $request, Merchant $merchant)
    {
        $merchant->update($request->validated());
        return true;
    }
    public function destroy(Merchant $merchant)
    {
        $merchant->delete();
        return true;
    }
}
