<?php

namespace App\Http\Controllers;

use App\Models\Region;
use Illuminate\Http\Request;
use App\Http\Requests\RegionRequest;
use DB;


class RegionController extends Controller
{
    public function index()
    {
        $dataClient = DB::table('client')->get();
        return view('contents.region.index', compact('dataClient'));
    }
    public function all(Request $request)
    {
        abort_if(!$request->header('X-XSRF-TOKEN'), 404);
        return Region::join('client', 'client.id', '=', 'region.client_id')
        ->get([
            'region.*',
            'client.client_name'
        ]);
    }
    public function store(RegionRequest $request)
    {
        Region::create($request->validated());
        return true;
    }
    public function update(RegionRequest $request, Region $region)
    {
        $region->update($request->validated());
        return true;
    }
    public function destroy(Region $region)
    {
        $region->delete();
        return true;
    }
}
