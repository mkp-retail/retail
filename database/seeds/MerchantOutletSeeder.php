<?php

use Illuminate\Database\Seeder;

class MerchantOutletSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('merchant_outlet')->insert([[
            'merchant_id' => 1,
            'merchant_outlet_name' => 'Outlet B2B 1',
        ],[
            'merchant_id' => 2,
            'merchant_outlet_name' => 'Outlet B2C 1',
        ],[
            'merchant_id' => 2,
            'merchant_outlet_name' => 'Outlet B2C 2',
        ]]);

        $faker = \Faker\Factory::create('id_ID');
        for ($i=0; $i <= 99; $i++) { 
            DB::table('merchant_outlet')->insert([
                'merchant_id' => $faker->numberBetween($min = 4, $max = 52),
                'merchant_outlet_name' => 'Outlet '.$faker->numberBetween($min = 1, $max = 10),
            ]);
        }
    }
}
