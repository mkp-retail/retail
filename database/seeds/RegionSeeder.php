<?php

use Illuminate\Database\Seeder;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('region')->insert([
            [
                'client_id' => 1,
                'region_name' => 'JAWA TENGAH',
            ],
            [
                'client_id' => 1,
                'region_name' => 'JAWA BARAT',
            ],
            [
                'client_id' => 2,
                'region_name' => 'JAWA TENGAH',
            ],
            [
                'client_id' => 2,
                'region_name' => 'JAWA BARAT',
            ],
            [
                'client_id' => 3,
                'region_name' => 'JAWA TENGAH',
            ],
            [
                'client_id' => 3,
                'region_name' => 'JAWA BARAT',
            ]
         ]);
    }
}
