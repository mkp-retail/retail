<?php

use Illuminate\Database\Seeder;

class MerchantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('merchant')->insert([[
            'group_id'                  => 1,
            'village_id'                => '3374130009',
            'merchant_name'             => 'MKP B2B - TUNGGAL',
            'merchant_nik'              => '1234567890',
            'merchant_address'          => strtoupper('Jl. Singosari I No.12, Pleburan, Kec. Semarang Sel., Kota Semarang, Jawa Tengah 50241'),
            'merchant_telp'             => '(024) 234562345',
            'merchant_email'            => 'mkpmobile.b2b@gmail.com',
            'merchant_date_of_birth'    => '2018-05-01',
            'merchant_pic_name'         => 'AULIA W',
            'merchant_pic_telp'         => '089923456345',
            'created_at'                => now(),
        ],[
            'group_id'                  => 2,
            'village_id'                => '3374130009',
            'merchant_name'             => 'MKP B2C - GANDA',
            'merchant_nik'              => '0987654321',
            'merchant_address'          => strtoupper('Jl. Singosari I No.12, Pleburan, Kec. Semarang Sel., Kota Semarang, Jawa Tengah 50241'),
            'merchant_telp'             => '(024) 234562345',
            'merchant_email'            => 'mkpmobile.b2c@gmail.com',
            'merchant_date_of_birth'    => '2018-05-01',
            'merchant_pic_name'         => 'AULIA W',
            'merchant_pic_telp'         => '089923456345',
            'created_at'                => now(),
        ]]);
        $faker = \Faker\Factory::create('id_ID');
        for ($i=0; $i < 50; $i++) { 
            DB::table('merchant')->insert([
                'group_id'                  => $faker->numberBetween($min = 3, $max = 10),
                'village_id'                => '3374130009',
                'merchant_name'             => strtoupper($faker->company),
                'merchant_nik'              => $faker->numberBetween($min = 1234000000000001, $max = 1234000099999999),
                'merchant_address'          => strtoupper($faker->address),
                'merchant_telp'             => $faker->phoneNumber,
                'merchant_email'            => $faker->email,
                'merchant_date_of_birth'    => $faker->dateTimeBetween('-1 months'),
                'merchant_pic_name'         => strtoupper($faker->name),
                'merchant_pic_telp'         => '0'.$faker->numberBetween($min = 81, $max = 89).$faker->numberBetween($min = 100, $max = 999).$faker->numberBetween($min = 101, $max = 99999),
                'created_at'                => now(),
            ]);
        }
    }
}
