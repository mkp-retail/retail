<?php

use Illuminate\Database\Seeder;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (['MKP BILLER', 'INDOGROSIR', 'LOTTE MART RETAIL', 'SAMPOERNA RETAIL COMMUNITY', 'GUDANG GARAM STRATEGIC PARTNERSHIP'] as $value) {
            DB::table('client')->insert([
                'client_name'       => $value,
                'created_at'        => now()
            ]);
        }
    }
}
