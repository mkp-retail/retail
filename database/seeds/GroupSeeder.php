<?php

use Illuminate\Database\Seeder;

class GroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('group')->insert([[
            'region_id'         => 1,
            'group_name'        => 'MKP B2B',
            'created_at'        => now(),
        ],[
            'region_id'         => 2,
            'group_name'        => 'MKP B2C',
            'created_at'        => now(),
        ],]);

        $faker = \Faker\Factory::create('id_ID');
        for ($i=0; $i < 9; $i++) { 
            DB::table('group')->insert([
                'region_id'         => $faker->numberBetween($min = 3, $max = 6),
                'group_name'        => strtoupper($faker->ean13),
                'created_at'        => now(),
            ]);
        }
    }
}
