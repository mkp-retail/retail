<?php

use Illuminate\Database\Seeder;

class InitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ulevel')->insert([[
            'level'			=> 1,
            'uraianlevel'	=> 'sys',
            'rule'			=> 'sys',
            'created_at'	=> now(),
            'updated_at'	=> now()
		],[
            'level'			=> 2,
            'uraianlevel'	=> 'level',
            'rule'			=> 'level',
            'created_at'	=> now(),
            'updated_at'	=> now()
		]]);
		
        DB::table('users')->insert([[
            'username'		        => 'sys',
            'email'                 => 'sys',
            'password'		        => 'sys',
            'level_tree'            => 999,
            'client_id'             => null,
            'region_id'             => null,
            'group_id'              => null,
            'level_id'              => 1,
            'created_at'	        => now(),
            'updated_at'	        => now()
        ],[
            'username'		        => 'admin',
            'email'                 => 'admin@admin.com',
            'password'		        => bcrypt('admin'),
            'level_tree'            => 1,
            'client_id'             => null,
            'region_id'             => null,
            'group_id'              => null,
            'level_id'              => 2,
            'created_at'	        => now(),
            'updated_at'	        => now()
        ]]);
    }
}
