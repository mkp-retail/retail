<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       \Artisan::call('laravolt:indonesia:seed');

        $this->call(InitSeeder::class);
        $this->call(ClientSeeder::class);
        $this->call(RegionSeeder::class);
        $this->call(GroupSeeder::class);
        $this->call(MerchantSeeder::class);
        $this->call(MerchantOutletSeeder::class);
        $this->call(AccountSeeder::class);
        $this->call(TransactionCodeSeeder::class);        
        $this->call(TransactionSeeder::class);        
        $this->call(BillerSeeder::class);        
    }
}
