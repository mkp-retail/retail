<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSegmentationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchant_segment', function (Blueprint $table) {
            $table->increments('id');
            $table->string('merchant_segment_code',16);
            $table->string('merchant_segment_name',32);
            $table->timestamps();
        });
        Schema::create('merchant_segment_list_payment', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('merchant_segment_id');
            $table->unsignedInteger('payment_method_id');
            $table->timestamps();
            $table->foreign('merchant_segment_id')->references('id')->on('merchant_segment');
            $table->foreign('payment_method_id')->references('id')->on('payment_method');

        });
        Schema::create('payment_method', function (Blueprint $table) {
            $table->increments('id');
            $table->string('payment_method_name',32);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('segmentation');
    }
}
