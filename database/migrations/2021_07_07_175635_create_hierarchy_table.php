<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHierarchyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   //hierarchy
        Schema::create('client', function (Blueprint $table) {
            $table->increments('id');
            $table->string('client_name',128);
            $table->timestamps();
        });
        Schema::create('region', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('client_id');
            $table->string('region_name',128);
            $table->timestamps();
            $table->foreign('client_id')->references('id')->on('client');
        });
        Schema::create('group', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('region_id');
            $table->string('group_name',128);
            $table->timestamps();
            $table->foreign('region_id')->references('id')->on('region');
        });
        Schema::create('merchant', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('merchant_segment_id');
            $table->unsignedInteger('group_id');
            $table->char('village_id', 10);
            $table->string('merchant_name',128);
            $table->string('merchant_nik',128)->nullable();
            $table->string('merchant_address',255)->nullable();
            $table->string('merchant_telp',32)->nullable();
            $table->string('merchant_email',128)->nullable();
            $table->date('merchant_date_of_birth', 0)->nullable();
            $table->string('merchant_pic_name',128)->nullable();
            $table->string('merchant_pic_telp',32)->nullable();
            $table->string('merchant_env_payment_mid',128)->nullable();
            $table->timestamps();
            $table->foreign('group_id')->references('id')->on('group');
            $table->foreign('village_id')->references('id')->on('indonesia_villages');
            $table->foreign('merchant_segment_id')->references('id')->on('merchant_segment');

        });
        Schema::create('merchant_outlet', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('merchant_id');
            $table->string('merchant_outlet_name',128);
            $table->timestamps();
            $table->foreign('merchant_id')->references('id')->on('merchant');
            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client');
        Schema::dropIfExists('region');
        Schema::dropIfExists('group');
        Schema::dropIfExists('merchant');
        Schema::dropIfExists('merchant_outlet');
    }
}
