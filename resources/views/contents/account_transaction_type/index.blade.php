@extends('layouts.template', [
    'pageTitle' => 'Account Transaction Type',
    'activeMenu' => 'accountTransactionType'
])

@section('breadcrumb')
<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
	<div class="d-flex">
		<div class="breadcrumb">
			<a href="{{ route('dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
			<span class="breadcrumb-item">Account</span>
			<span class="breadcrumb-item active">Transaction Type</span>
		</div>
	</div>
</div>
@endsection

@section('content')
<!-- Basic datatable -->
<div class="card">
	<div class="card-header header-elements-inline">
        <h5 class="card-title">List Transaction Type</h5>
        <div class="header-elements">
            <button type="button" @click="createModal" class="btn bg-teal-400 btn-labeled btn-labeled-left rounded-round"><b><i class="icon-plus-circle2"></i></b> Add Data</button>
        </div>
	</div>

	<table class="table table-hover table-striped table-xs text-nowrap" id="datatable_basic">
		<thead>
			<tr>
				<th>#</th>
                <th class="text-center">Actions</th>
                <th>Transaction Code</th>
                <th>Transaction Name</th>
                <th>Transaction Debits Credits</th>
                <th>Created At</th>
                <th>Updated At </th>
			</tr>
		</thead>
		<tbody>
			<tr v-if="mainData" v-for="item, index in mainData" :key="index">
				<td v-text="index+1"></td>
                <td style="width: 16px;">
                    <div class="btn-group">
                        <button type="button" class="btn btn-outline bg-primary-400 text-primary-400 btn-icon btn-sm rounded-round ml-2"
                            data-popup="tooltip" title="" data-placement="left" data-original-title="Edit" @click="editModal(item)">
                            <i class="fas fa-pencil-alt"></i></button>
                        <button type="button" class="btn btn-outline bg-danger-400 text-danger-400 btn-icon btn-sm rounded-round ml-2"
                            data-popup="tooltip" title="" data-placement="left" data-original-title="Delete" @click="deleteData(item.id)">
                            <i class="far fa-trash-alt"></i></button>
                    </div>
                </td>
                <td v-text="item.account_transaction_type_code"></td>
                <td v-text="item.account_transaction_type_desc"></td>
                <td v-text="item.account_transaction_type_dk"></td>
                <td v-text="item.created_at"></td>
                <td v-text="item.updated_at"></td>
			</tr>
		</tbody>
	</table>
</div>
<!-- /basic datatable -->

<!-- Basic modal -->
<div id="modal_default" class="modal fade" tabindex="-1">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header" :class="editMode ? 'bg-primary' : 'bg-teal'">
                <h4 class="modal-title">@{{ (editMode ? 'Edit Data' : 'New Data') }}</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<form @submit.prevent="editMode ? updateData() : storeData()" @keydown="form.onKeydown($event)">
				<div class="modal-body">
                    <div class="form-group">
                        <label for="account_transaction_type_code">Transaction Code</label>
                        <input id="account_transaction_type_code" type="text" placeholder="Input Transaction Code" class="form-control"
                            v-model="form.account_transaction_type_code" :class="{ 'is-invalid': form.errors.has('account_transaction_type_code') }">
                            <has-error :form="form" field="account_transaction_type_code"></has-error>
                    </div>
                    <div class="form-group">
                        <label for="account_transaction_type_desc">Transaction Description</label>
                        <input id="account_transaction_type_desc" type="text" placeholder="Input Transaction Descriptions" class="form-control"
                            v-model="form.account_transaction_type_desc" :class="{ 'is-invalid': form.errors.has('account_transaction_type_desc') }">
                            <has-error :form="form" field="account_transaction_type_desc"></has-error>
                    </div>
                    <div class="form-group">
                        <label for="account_transaction_type_dk">Debits Or Credits</label>
                        <select id="account_transaction_type_dk" class="form-control select-search" data-placeholder="your choice..." onchange="app.select2Changed('account_transaction_type_dk', this.value)">
                            <option value=""></option>
                            <option value="D">Debits</option>
                            <option value="C">Credits</option>
                        </select>
                        <span class="manual-error" v-if="form.errors.has('account_transaction_type_dk')">Pilih Jenis</span>
                    </div>
				</div>

				<div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-ladda btn-ladda-submit" :class="editMode ? 'bg-primary' : 'bg-teal-600'">
						@{{ editMode ? 'Update' : 'Save' }}
					</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- /basic modal -->
@endsection

@push('js')
<script>
    var app = new Vue({
        el: '#app',
        data: {
        	mainData: [],
            editMode : false,
            form : new Form({
                id: '',
                account_transaction_type_code: '',
                account_transaction_type_desc: '',
                account_transaction_type_dk: '',
            }),
        },
        mounted() {
	        $('#datatable_basic').DataTable();
	        this.refreshDatatable();
        },
        watch: {
            form: {
                handler(val){
                    // detect select2 fail validation
                    var error = val.errors.errors;
                    select2Error(val = ('account_transaction_type_dk' in error), indexSelect2 = 1)
                },
                deep: true,
            }
        },
        methods: {
            select2Changed(field, value){
                if (field == 'account_transaction_type_dk') return this.form.account_transaction_type_dk = value
            },
            createModal(){
                this.editMode = false;
                this.form.reset();
                this.form.clear();
               $('#account_transaction_type_dk').val('').trigger('change');
                $('#modal_default').modal('show');
            },
            editModal(data){
                this.editMode = true;
                this.form.fill(data);
                this.form.clear();
               $('#account_transaction_type_dk').val(data.account_transaction_type_dk).trigger('change');
                $('#modal_default').modal('show');
            },
            storeData(){
                l = laddaButton('.btn-ladda-submit')
                l.start();

                this.form.post("{{ route('accountTransactionType.store') }}")
                    .then(response => {
                        basicFormSuccess('Data telah disimpan')
                        this.refreshDatatable()
                        setTimeout(() => { $('#modal_default').modal('hide') }, 1500);
                    })
                    .catch(e => {
                        basicFormError(e)
                    })
                    .finally(ok => {
                        setTimeout(() => { l.setProgress(0.5) }, 500);
                        setTimeout(() => { l.stop(); l.remove() }, 1300);
                    })
            },
            updateData(){
                l = laddaButton('.btn-ladda-submit')
                l.start();

                url = "{{ route('accountTransactionType.update', ':id') }}".replace(':id', this.form.id)
                this.form.put(url)
                    .then(response => {
                        basicFormSuccess('Data telah diupdate')
                        this.refreshDatatable()
                        setTimeout(() => { $('#modal_default').modal('hide') }, 1500);
                    })
                    .catch(e => {
                        basicFormError(e)
                    })
                    .finally(ok => {
                        setTimeout(() => { l.setProgress(0.5) }, 500);
                        setTimeout(() => { l.stop(); l.remove() }, 1300);
                    })
            },
            deleteData(id){
                DeleteConfirmation.fire().then((result) => {
                    if (result.value) {
                        url = "{{ route('accountTransactionType.destroy', ':id') }}".replace(':id', id)
                        axios.delete(url)
                            .then(response => {
                                basicFormSuccess('Data berhasil dihapus')
                                this.refreshDatatable()
                            })
                            .catch(e => {
                                basicFormError(e)
                            })
                    }
                })
            },
            refreshDatatable(){
            	axios.get("{{ route('accountTransactionType.all') }}")
                    .then(response => {
                        $('#datatable_basic').DataTable().destroy();
                        this.mainData = response.data
                        this.$nextTick(function() {
                            $('#datatable_basic').DataTable({
                                dom: '<"datatable-header "fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                                drawCallback: function(){
                                    $('[data-popup="tooltip"]').tooltip({trigger : 'hover'});
                                }
                            });
                            redrawTable();
                        })
                    })
                    .catch(e => {
                        basicFormError(e)
                    })
            }
        },
    })
</script>
@endpush