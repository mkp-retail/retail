@extends('layouts.template', [
    'pageTitle' => 'Transaction Report',
    'activeMenu' => 'reportTransaction',
    
])

@section('breadcrumb')
<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
	<div class="d-flex">
		<div class="breadcrumb">
			<a href="{{ route('dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
			<span class="breadcrumb-item">Biller</span>
			<span class="breadcrumb-item">Report</span>
			<span class="breadcrumb-item active">Transaction Report</span>
		</div>
	</div>
</div>
@endsection

@section('content')
<!-- Basic datatable -->
<div class="card">
	<div class="card-header header-elements-inline">
        <h5 class="card-title">List Biller Transaction Report</h5>
	</div>

    <div class="card-body">
        <form @submit.prevent="refreshDatatable()" @keydown="form.onKeydown($event)">
            <hr>
            <h6 class="font-weight-bold mb-2">Filter Data:</h6>

            <div class="row mb-2">
                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                    <label>Start Date:</label>
                    <div class="input-group">
                        <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-calendar22"></i></span>
                        </span>
                        <input id="startDate" type="text" class="form-control daterange-single" :value="filterForm.startDate">
                    </div>
                </div>
                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                    <label>End Date:</label>
                    <div class="input-group">
                        <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-calendar22"></i></span>
                        </span>
                        <input id="endDate" type="text" class="form-control daterange-single" :value="filterForm.endDate">
                    </div>
                </div>
            </div>

			<div class="row">
                <div class="form-group col-md-4 col-sm-6 col-xs-12 d-flex">
                    <button type="submit" class="btn bg-primary btn-block mt-auto btn-ladda btn-ladda-submitt">Filter</button>
                </div>
                <div class="col-md-4 col-sm-6 mt-0 mb-3">
                    <button type="button" class="btn bg-success-800 btn-block btn-ladda btn-ladda-submit" @click="exportExcel">Export Excel</button>
                </div>
			</div>
        </form>
    </div>

	<table class="table table-hover table-striped table-xs text-nowrap" id="datatable_basic">
		<thead>
			<tr>
                <th>#</th>
                <th>Client Name</th>
                <th>Region Name</th>
                <th>Group Name</th>
                <th>Merchant Name</th>
                <th>Merchant Outlet Name</th>
                <th>Product Type Name</th>
                <th>Product Category Name</th>
                <th>Product Unit Name</th>
                <th>Product Code</th>
                <th>Product Name</th>
                <th>Product Price</th>
                <th>Product Admin fee</th>
                <th>Product Merchant fee</th>
                <th>Product Provider Code</th>
                <th>Product Provider Name</th>
                <th>Product Provider Price</th>
                <th>Product Provider Admin Fee</th>
                <th>Product Provider Merchant Fee</th>
                <th>Transaction Number</th>
                <th>Transaction Number Merchant</th>
                <th>Transaction Number Merchant Index</th>
                <th>Transaction Number Provider</th>
                <th>Transaction Number Provider Index</th>
                <th>Created At</th>
			</tr>
		</thead>
	</table>
</div>
<!-- /basic datatable -->
@endsection

@push('js')
<script src="{{ asset('template/global_assets/js/plugins/pickers/daterangepicker.js') }}"></script>
<script>
    var app = new Vue({
        el: '#app',
        data: {
        	mainData: [],
            mainDataDetail: [],
            editMode : false,
            filterForm : new Form({
                startDate: moment().add(-30, 'day').format("YYYY-MM-DD"),
                endDate: moment().format("YYYY-MM-DD"),
            }),
        },
        mounted() {
            $('.daterange-single').daterangepicker({ 
                timePicker: false,
                singleDatePicker: true,
                locale: {
                    format: 'YYYY-MM-DD'
                }
            });
            $('.select-search').select2();
	        $('#datatable_basic').DataTable();
	        this.refreshDatatable();
        },
        
        methods: {
            select2Changed(field, value){
                if (field == 'merchant_id') return this.form.merchant_id = value
            },
            exportExcel(){
                l = laddaButton('.btn-ladda-submit')
                l.start();
                axios({
                    url: "{{ route('reportTransaction.export_excel') }}",
                    method: 'post',
                    responseType: 'blob', // important
                    data: {filter: this.filterForm},
                }).then((response) => {
                    toast('success', 'Data Exported')
                    const url = window.URL.createObjectURL(new Blob([response.data]));
                    const link = document.createElement('a');
                    link.href = url;
                    link.setAttribute('download', 'Data Reporting Retail ('+this.filterForm.startDate +'-'+ this.filterForm.endDate+').xlsx'); 
                    document.body.appendChild(link);
                    link.click();
                })
                .catch(e => {
                    basicFormError(e);
                })
                .finally(ok => {
                        setTimeout(() => { l.setProgress(0.5) }, 500);
                        setTimeout(() => { l.stop(); l.remove() }, 1300);
                    })
            },
            refreshDatatable(){
                l = laddaButton('.btn-ladda-submitt')
                l.start();
                this.filterForm.startDate = $('#startDate').val();
                this.filterForm.endDate = $('#endDate').val();
                this.filterForm.post("{{ route('reportTransaction.check_daterange') }}")
                    .then(response => {
                        toast('success', 'Filter applied')
                        $('#datatable_basic').DataTable().destroy();
                        this.$nextTick(function() {
                            $('#datatable_basic').DataTable({
                                dom: '<"datatable-header "fbl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                                processing: true,
                                serverSide: true,
                                order: [ [4, 'desc'] ], // order by tanggal
                                ajax: {
                                    url: "{{ route('reportTransaction.all') }}",
                                    headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
                                    type: 'POST',
                                    data: {filter: this.filterForm},
                                    error: function (xhr, error, code) { swal.fire( 'Gagal!', xhr.responseJSON.message, 'error' ) }
                                },
                                columns: [
                                    { data: 'DT_RowIndex', orderable: false, searchable: false },
                                    { data: 'client_name', name: 'client_name' },
                                    { data: 'region_name', name: 'region_name' },
                                    { data: 'group_name', name: 'group_name' },
                                    { data: 'merchant_name', name: 'merchant_name' },
                                    { data: 'merchant_outlet_name', name: 'merchant_outlet_name' },
                                    { data: 'product_type_name', name: 'product_type_name' },
                                    { data: 'product_category_name', name: 'product_category_name' },
                                    { data: 'product_unit_name', name: 'product_unit_name' },
                                    { data: 'product_code', name: 'product_code' },
                                    { data: 'product_name', name: 'product_name' },
                                    { data: 'product_price', name: 'product_price' },
                                    { data: 'product_admin_fee', name: 'product_admin_fee' },
                                    { data: 'product_merchant_fee', name: 'product_merchant_fee' },
                                    { data: 'product_provider_code', name: 'product_provider_code' },
                                    { data: 'product_provider_name', name: 'product_provider_name' },
                                    { data: 'product_provider_price', name: 'product_provider_price' },
                                    { data: 'product_provider_admin_fee', name: 'product_provider_admin_fee' },
                                    { data: 'product_provider_merchant_fee', name: 'product_provider_merchant_fee' },
                                    { data: 'transaction_number', name: 'transaction_number' },
                                    { data: 'transaction_number_merchant', name: 'transaction_number_merchant' },
                                    { data: 'transaction_number_merchant_index', name: 'transaction_number_merchant_index' },
                                    { data: 'transaction_number_provider', name: 'transaction_number_provider' },
                                    { data: 'transaction_number_provider_index', name: 'transaction_number_provider_index' },
                                    { data: 'created_at', name: 'created_at' },
                                ],
                                drawCallback: function(){
                                    $('[data-popup="tooltip"]').tooltip({trigger : 'hover'});
                                },
                                initComplete: function(data){
                                    // app.updateTableSuccess(data.json.payload);
                                }
                            });
                            redrawTable();
                        })

                    })
                    .catch(e => {
                        basicFormError(e)
                    })
                    .finally(ok => {
                        setTimeout(() => { l.setProgress(0.5) }, 500);
                        setTimeout(() => { l.stop(); l.remove() }, 1300);
                    })
            }
        },
    })
</script>
@endpush