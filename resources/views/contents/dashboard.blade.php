@extends('layouts.template', ['activeMenu' => 'dashboard'])

@section('breadcrumb')
<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
	<div class="d-flex">
		<div class="breadcrumb">
			<span class="breadcrumb-item active"><i class="icon-home2 mr-2"></i> Dashboard</span>
		</div>

		<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
	</div>
</div>
@endsection

@section('content')
<div class="row">
	<div class="col-sm-6 col-xl-3">
		<div class="card card-body bg-blue-400 has-bg-image">
			<div class="media">
				<div class="media-body">
					<h3 class="mb-0">54</h3>
					<span class="text-uppercase font-size-xs">total kabupaten</span>
				</div>

				<div class="ml-3 align-self-center">
					<i class="icon-bubbles4 icon-3x opacity-75"></i>
				</div>
			</div>
		</div>
	</div>

	<div class="col-sm-6 col-xl-3">
		<div class="card card-body bg-danger-400 has-bg-image">
			<div class="media">
				<div class="media-body">
					<h3 class="mb-0">438</h3>
					<span class="text-uppercase font-size-xs">total merchant</span>
				</div>

				<div class="ml-3 align-self-center">
					<i class="icon-bag icon-3x opacity-75"></i>
				</div>
			</div>
		</div>
	</div>

	<div class="col-sm-6 col-xl-3">
		<div class="card card-body bg-success-400 has-bg-image">
			<div class="media">
				<div class="mr-3 align-self-center">
					<i class="icon-pointer icon-3x opacity-75"></i>
				</div>

				<div class="media-body text-right">
					<h3 class="mb-0">652,549</h3>
					<span class="text-uppercase font-size-xs">total outlet</span>
				</div>
			</div>
		</div>
	</div>

	<div class="col-sm-6 col-xl-3">
		<div class="card card-body bg-indigo-400 has-bg-image">
			<div class="media">
				<div class="mr-3 align-self-center">
					<i class="icon-enter6 icon-3x opacity-75"></i>
				</div>

				<div class="media-body text-right">
					<h3 class="mb-0">245,382</h3>
					<span class="text-uppercase font-size-xs">total penjualan</span>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="card">
	<div class="card-header header-elements-inline">
		<h5 class="card-title">Laporan Penjualan Tahun 2020</h5>
		<div class="header-elements">
			<div class="list-icons">
				<a class="list-icons-item" data-action="collapse"></a>
				<a class="list-icons-item" data-action="reload"></a>
				<a class="list-icons-item" data-action="remove"></a>
			</div>
		</div>
	</div>

	<div class="card-body">
		<div class="chart-container">
			<div class="chart has-fixed-height" id="pie_rose_labels" style="user-select: none; position: relative;" _echarts_instance_="ec_1597023129117"><div style="position: relative; overflow: hidden; width: 476px; height: 400px; padding: 0px; margin: 0px; border-width: 0px; cursor: pointer;"><canvas style="position: absolute; left: 0px; top: 0px; width: 476px; height: 400px; user-select: none; padding: 0px; margin: 0px; border-width: 0px;" data-zr-dom-id="zr_0" width="476" height="400"></canvas></div><div></div></div>
		</div>
	</div>
</div>
@endsection

@push('js')
<script src="{{ asset('template/global_assets/js/plugins/visualization/echarts/echarts.min.js') }}"></script>
<script src="{{ asset('template/global_assets/js/demo_pages/charts/echarts/pies_donuts.js') }}"></script>
<script>
    var app = new Vue({
        el: '#app',
    })
</script>
@endpush