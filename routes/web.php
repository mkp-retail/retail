<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return redirect()->route('dashboard'); });
Route::group(['middleware' => ['web', 'auth']], function () {
    
    Route::get('dashboard', 'DashboardController')->name('dashboard');
    
    Route::post('ajax_select/{type}', 'AjaxSelectController')->name('ajaxSelect');

	Route::get('user/all', 'UserController@all')->name('user.all');
    Route::resource('user', 'UserController')->except('create', 'edit', 'show');
	
    Route::get('client/all', 'ClientController@all')->name('client.all');
    Route::resource('client', 'ClientController')
        ->parameters(['client' => 'client'])->except('create', 'edit', 'show');
	
    Route::get('region/all', 'RegionController@all')->name('region.all');
    Route::resource('region', 'RegionController')
        ->parameters(['region' => 'region'])->except('create', 'edit', 'show');
	
    Route::get('group/all', 'GroupController@all')->name('group.all');
    Route::resource('group', 'GroupController')
        ->parameters(['group' => 'group'])->except('create', 'edit', 'show');
	
    Route::post('merchant/all', 'MerchantController@all')->name('merchant.all');
    Route::resource('merchant', 'MerchantController')
        ->parameters(['merchant' => 'merchant'])->except('create', 'edit', 'show');
	
    Route::post('merchantOutlet/all', 'MerchantOutletController@all')->name('merchantOutlet.all');
    Route::resource('merchantOutlet', 'MerchantOutletController')
        ->parameters(['merchantOutlet' => 'merchantOutlet'])->except('create', 'edit', 'show');
	

});

Auth::routes([
    'register'  => true,
    'verify'    => false,
    'reset'     => false,
    'confirm'   => false,
]);
